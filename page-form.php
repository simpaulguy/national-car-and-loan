<?php

/**
 * Template Name: Form Page Template
 *
 * This is the page template for form related pages. This file assumes that nothing has been moved
 * from the Genesis default.
 *
 * @category   Genesis_Sandbox
 * @package    Templates
 * @subpackage Page
 * @author     Travis Smith
 * @license    http://www.opensource.org/licenses/gpl-license.php GPL v2.0 (or later)
 * @link       http://wpsmith.net/
 * @since      1.1.0
 */

/** Exit if accessed directly */
if ( ! defined( 'ABSPATH' ) ) exit( 'Cheatin&#8217; uh?' );

// Add page specific body class
add_filter( 'body_class', 'fms_mortgage_refinance_articles_body_class' );
function fms_mortgage_refinance_articles_body_class( $classes ) {
   $classes[] = 'page-form-template';
   return $classes;
}

// Disable WordPress autop
remove_filter('the_content', 'wpautop');

// Remove page title
remove_action('genesis_entry_header', 'genesis_do_post_title');

// Remove default featured image
remove_action( 'genesis_before_entry', 'post_featured_image' );

// Remove after content widget area
remove_action( 'genesis_after_content', 'ncl_after_content_widget_area', 12 );

// Remove link on the logo
add_filter( 'genesis_seo_title', 'child_header_title', 10, 3 );
function child_header_title( $title, $inside, $wrap ) {
   	$inside = sprintf( '%s', esc_attr( get_bloginfo( 'name' ) ), get_bloginfo( 'name' ) );
   	return sprintf( '<%1$s class="site-title">%2$s</%1$s>', $wrap, $inside );
}

genesis();
